from django.conf import settings
from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from blog.post.views import PostsListView

urlpatterns = [
    url(r'^$', PostsListView.as_view(), name='home'),
    # url(r'^note/(?P<pk>[0-9]+)$', NoteDetail.as_view(), name='note_detail'),
    # url(r'^tag/(?P<tag_name>\w+)$', TagDetailView.as_view(), name='tag_detail'),
    url(r'^admin/', admin.site.urls),
    url(r'^taggit_autosuggest/', include('taggit_autosuggest.urls')),
    url(r'^filer/', include('filer.urls')),

]

urlpatterns += staticfiles_urlpatterns()

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
