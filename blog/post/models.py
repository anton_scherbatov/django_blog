from __future__ import unicode_literals

import os

from filer.fields.file import FilerFileField
from filer.fields.image import FilerImageField
from taggit.models import Tag, GenericTaggedItemBase
from taggit_autosuggest.managers import TaggableManager

from django.db.models import CharField, BooleanField, DateTimeField, ForeignKey, TextField
from django.contrib.auth.models import User
from django.db import models
from django.utils.text import slugify
from transliterate import translit


class TagSlugify(Tag):
    def slugify(self, tag, i=None):
        return slugify(translit(tag, 'ru', reversed=True))


class TaggedItemSlugify(GenericTaggedItemBase):
    tag = models.ForeignKey(TagSlugify, related_name="%(app_label)s_%(class)s_items", on_delete=models.CASCADE)


class Post(models.Model):
    title = CharField(max_length=255)
    author = ForeignKey(User)
    short_text = TextField(blank=True)
    full_text = TextField(blank=True)
    date_created = DateTimeField(auto_now_add=True)
    enabled = BooleanField()
    tags = TaggableManager(blank=True, through=TaggedItemSlugify)

    class Meta:
        ordering = ('-date_created',)


class Slide(models.Model):
    post = ForeignKey(Post, verbose_name='Images', blank=True)
    image = FilerImageField(null=True, blank=True)
    text = models.TextField(blank=True)


class Audio(models.Model):
    post = ForeignKey(Post, verbose_name='Audio', blank=True)
    file = FilerFileField(null=True, blank=True)
    title = models.TextField(blank=True)


class Video(models.Model):
    post = ForeignKey(Post, verbose_name='Video', blank=True)
    file = FilerFileField(null=True, blank=True, related_name='video_file')
    poster = FilerImageField(null=True, blank=True, related_name='video_poster')
    title = models.TextField(blank=True)
    artist = models.TextField(blank=True)
