from collections import defaultdict, OrderedDict
from django.views.generic import ListView

from blog.post.models import Post


class PostsListView(ListView):
    template_name = 'post/list.html'
    model = Post

    def get_queryset(self):
        qs = super(PostsListView, self).get_queryset()
        qs = qs.select_related('author')
        qs = qs.prefetch_related('tags', 'slide_set')
        return qs.filter(enabled=True)

    def get_context_data(self, **kwargs):
        context = super(PostsListView, self).get_context_data(**kwargs)
        posts_groupped_by_date = OrderedDict()
        for post in self.object_list:
            key = post.date_created.strftime('%Y-%m-%d')
            posts_groupped_by_date.setdefault(key, [])
            posts_groupped_by_date[key].append(post)

        for date, posts in posts_groupped_by_date.items():
            his_posts, her_posts, full_posts = [], [], []  # three types of posts
            posts_for_date = []
            for post in posts:
                username = post.author.username
                if username == 'admin':  # full post
                    if his_posts or her_posts:
                        posts_for_date.append(dict(her_posts=her_posts, his_posts=his_posts))
                        his_posts, her_posts = [], []
                    full_posts.append(post)
                else:  # left or right post
                    if full_posts:
                        posts_for_date.append(dict(full_posts=full_posts))
                        full_posts = []
                    if username == 'storvus':
                        his_posts.append(post)
                    else:
                        her_posts.append(post)

            if full_posts:
                posts_for_date.append(dict(full_posts=full_posts))
            if his_posts or her_posts:
                posts_for_date.append(dict(her_posts=her_posts, his_posts=his_posts))

            posts_groupped_by_date[date] = posts_for_date
        print posts_groupped_by_date
        context['posts_by_date'] = posts_groupped_by_date
        return context
