from django.contrib import admin
from django.db import models

from pagedown.widgets import AdminPagedownWidget
from tabbed_admin import TabbedModelAdmin

from blog.post.models import Post, Slide, Video, Audio


class SliderInline(admin.TabularInline):
    model = Slide

class AudioInline(admin.TabularInline):
    model = Audio

class VideoInline(admin.TabularInline):
    model = Video


class PostAdmin(TabbedModelAdmin):
    list_display = ('title', 'author', 'date_created', 'enabled', 'position')
    readonly_fields = ('date_created', )
    formfield_overrides = {
        models.TextField: {'widget': AdminPagedownWidget},
    }
    actions = ['enable_posts', 'disable_posts']

    tab_overview = (
        (None, {
            'fields': ('title', 'author', 'short_text', 'full_text', 'date_created', 'enabled', 'tags')
        }),
    )
    tab_slider = (
        SliderInline,
    )
    tab_audio = (
        AudioInline,
    )
    tab_video = (
        VideoInline,
    )
    tabs = [
        ('Overview', tab_overview),
        ('Slider', tab_slider),
        ('Audio', tab_audio),
        ('Video', tab_video)
    ]

    def position(self, obj):
        username = obj.author.username
        if username == 'storvus':
            return 'right'
        elif username == 'admin':
            return 'full'
        else:
            return 'left'

    def enable_posts(self, request, queryset):
        queryset.update(enabled=True)

    def disable_posts(self, request, queryset):
        queryset.update(enabled=False)


admin.site.register(Post, PostAdmin)
