from django import template
from taggit.models import Tag

register = template.Library()


@register.assignment_tag()
def get_tags():
    return Tag.objects.all().values_list('name', flat=True)
